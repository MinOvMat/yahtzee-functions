import { handleDiceErrors } from "./dices-error";
const getAcesValue = (dices) => {
    handleDiceErrors(dices);
    return dices.filter((dice) => dice === 1).length;
};
export { getAcesValue };
