class WrongNumberOfDicesError extends Error {
    constructor(message) {
        super(message || 'Exactly five dices are needed.');
    }
}
class NoFloatDiceError extends Error {
    constructor() {
        super('No floating point numbers in on dices.');
    }
}
export function handleDiceErrors(dices) {
    if (dices.length !== 5) {
        throw new WrongNumberOfDicesError(`Five dices are needed. You gave ${dices.length}.`);
    }
    if (dices.some((dice) => Math.round(dice) !== dice)) {
        throw new NoFloatDiceError();
    }
}
