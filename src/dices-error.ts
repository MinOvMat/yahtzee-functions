class WrongNumberOfDicesError extends Error {
  constructor(message?: string) {
    super(message || 'Exactly five dices are needed.');
  }
}
class NoFloatDiceError extends Error{
  constructor() {
    super('No floating point numbers in on dices.');
  }
}
class OutOfRangeDiceError extends Error{
  constructor() {
    super('Only values between 1 and 6 allowed.');
  }
}

export function handleDiceErrors(dices: number[]) {
  if (dices.length !== 5) {
    throw new WrongNumberOfDicesError(`Five dices are needed. You gave ${dices.length}.`)
  }
  if (dices.some((dice) => Math.round(dice) !== dice)) {
    throw new NoFloatDiceError()
  }
  if(dices.some(dice => dice < 1 || dice > 6)){
    throw new OutOfRangeDiceError()
  }
}
