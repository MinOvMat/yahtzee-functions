import {handleDiceErrors} from "./dices-error"

const getNthValue = (dices: number[], n:number):number => {
  handleDiceErrors(dices)
  return dices.filter((dice) => dice === n).length * n
}

const getAcesValue = (dices:number[]):number => {
  return getNthValue(dices, 1)
}

const getTwosValue = (dices:number[]):number => {
  return getNthValue(dices, 2)
}

const getThreesValue = (dices:number[]):number => {
  return getNthValue(dices, 3)
}

const getFoursValue = (dices:number[]):number => {
  return getNthValue(dices, 4)
}

const getFivesValue = (dices:number[]):number => {
  return getNthValue(dices, 5)
}

const getSixesValue = (dices:number[]):number => {
  return getNthValue(dices, 6)
}


export {
  getAcesValue,
  getTwosValue,
  getThreesValue,
  getFoursValue,
  getFivesValue,
  getSixesValue,
  getNthValue
}
