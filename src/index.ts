export {
  getAcesValue,
  getTwosValue,
  getThreesValue,
  getFoursValue,
  getFivesValue,
  getSixesValue,
} from "./upper-section"
