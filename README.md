# yahtzee-functions

## Description
Some simple functions that return the value of the category or 0 if not applicable.

## Installation

````
npm i yathzee-functions
````
or
````
yarn add yathzee-functions
````

## Usage
````js
import { getYahtzeeValue } from "yahtzee-functions"
const dices = [6,6,6,6,6]
getYahtzeeValue(dices)
// returns 50
const dices = [1,3,4,3,2]
getYahtzeeValue(dices)
//returns 0
````

