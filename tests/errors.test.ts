import {describe, expect, test} from '@jest/globals'
import {handleDiceErrors} from "../src/dices-error";

describe('aces', () =>{

  test('should throw error if array has not 5 dices.', () =>{
    expect(() => {handleDiceErrors([1, 1])}).toThrow()
  })
  test('should throw error if array has not 5 dices.', () =>{
    expect(() => {handleDiceErrors([1, 1, 1, 1, 1, 1])}).toThrow()
  })
  test('should throw if float is given', () => {
    expect(() => { handleDiceErrors([1.4, 1, 1, 1, 1])})
      .toThrow(/float.*/)
  })
  test('should throw if float is given', () => {
    expect(() => { handleDiceErrors([7, 1, 1, 1, 1])})
      .toThrow()
  })
  test('should throw if float is given', () => {
    expect(() => { handleDiceErrors([0, 1, 1, 1, 1])})
      .toThrow()
  })
})
