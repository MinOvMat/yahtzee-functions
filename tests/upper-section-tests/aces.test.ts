import {describe, expect, test} from '@jest/globals'
import {getAcesValue} from '../../src'

describe('aces', () =>{
  const oneAceDices = [1,2,2,2,2]
  test('should return 1, if given only one 1 in array', () =>{
    expect(getAcesValue(oneAceDices)).toBe(1)
  })
  const twoAceDices = [1,1,2,2,2]
  test('should return 2 if given two 1s in array', () =>{
    expect(getAcesValue(twoAceDices)).toBe(2)
  })
  const threeAceDices = [1,1,2,2,1]
  test('should return 3 if given three 1s in array', () =>{
    expect(getAcesValue(threeAceDices)).toBe(3)
  })

  test('should throw error if array has not 5 dices.', () =>{
    expect(() => {getAcesValue([1, 1])}).toThrow()
  })
  test('should throw if float is given', () => {
    expect(() => { getAcesValue([1.4, 1, 1, 1, 1])})
      .toThrow(/float.*/)
  })
})
