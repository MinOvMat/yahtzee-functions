import {describe, expect, test} from '@jest/globals'
import {getTwosValue} from '../../src'

describe('twos', () =>{
  const fourTwosDices = [1,2,2,2,2]
  test('should return 1, if given only one 1 in array', () =>{
    expect(getTwosValue(fourTwosDices)).toBe(8)
  })
  const threeTwosDices = [1,1,2,2,2]
  test('should return 2 if given two 1s in array', () =>{
    expect(getTwosValue(threeTwosDices)).toBe(6)
  })


  test('should throw error if array has not 5 dices.', () =>{
    expect(() => {getTwosValue([1, 1])}).toThrow()
  })
  test('should throw if float is given', () => {
    expect(() => { getTwosValue([1.4, 1, 1, 1, 1])})
      .toThrow(/float.*/)
  })
})
