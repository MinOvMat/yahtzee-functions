import {describe, expect, test} from '@jest/globals'
import {getThreesValue} from "../../src";

describe('twos', () =>{
  const fourTwosDices = [3,2,2,2,2]
  test('should return 3, if given only one 3 in array', () =>{
    expect(getThreesValue(fourTwosDices)).toBe(3)
  })
  const threeTwosDices = [2,2,3,3,2]
  test('should return 6 if given two 3s in array', () =>{
    expect(getThreesValue(threeTwosDices)).toBe(6)
  })

  test('should return 0 if no threes are given',() =>{
    expect(getThreesValue([1,1,2,2,6])).toBe(0)
  })

  test('should throw error if array has not 5 dices.', () =>{
    expect(() => {getThreesValue([3, 3])}).toThrow()
  })

  test('should throw if float is given', () => {
    expect(() => { getThreesValue([3.4, 3, 3, 3, 3])})
      .toThrow(/float.*/)
  })
})
