import {describe, expect, test} from '@jest/globals'
import {getNthValue, getThreesValue} from "../../src/upper-section";

describe('getNthValue', () => {
  test('should return 1, if given only one 1 and n is 1 in array', () => {
    expect(getNthValue([1,1,1,1,1], 1)).toBe(5)
  })
  test('should return 8, if given four 2s and n is 2 in array', () => {
    expect(getNthValue([1, 2, 2, 2, 2], 2)).toBe(8)
  })
  test('should return 0 if no ns are given',() =>{
    expect(getNthValue([1,1,2,2,6], 5)).toBe(0)
  })
})
